\documentclass[a4paper]{article}
\usepackage[cm]{fullpage}
\usepackage{mathtools}
\usepackage[utf8]{inputenc}
\usepackage[round]{natbib}
\usepackage[normalem]{ulem}
\usepackage{amssymb}
\usepackage{bm}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{tikz}
\usepackage{subcaption}
\usepackage{placeins}

\usetikzlibrary{decorations.markings}

\bibliographystyle{plainnat}

\newcommand{\diff}[2]{\frac{\mathrm{d} #1}{\mathrm{d} #2}}
\newcommand{\pdiff}[2]{\frac{\partial #1}{\partial #2}}

\DeclareMathOperator*{\length}{length}
\DeclareMathOperator*{\sgn}{sgn}

\begin{document}
\title{Poro-elasticity -- the master plan}
\author{Matthias Heil \and Matthew Russell}
\maketitle
\section{Primary Quantities}
In this document all dimensional quantities are written with a superscript asterisk,
and the corresponding nondimensional quantities without.
\begin{itemize}
    \item \(\mathbf{u}^*\): Displacement of the solid skeleton.
    \item \(\mathbf{q}^*\): the average velocity of seepage (relative to the solid skeleton), measured over the area of the composite material %, i.e.
%$$
%|{\bf w^*}| = \frac{\mbox{volume flux of pore fluid through
%an area element in the composite material}}
%{\mbox{area of that element}}.
%$$
    \item \(p_\mathrm{pore}\): pressure in pore fluid.
    \item \ [\(p_\mathrm{solid}\); pressure in (incompressible) solid. (Only
        needed if solid is incompressible or near incompressible.)]
\end{itemize}

\section{Governing equations}
Equations are written in the form in which they appear (consistently!)
in \citet{simon1992} and \citet{zienkiewicz1984}.
\begin{itemize}
    \item Momentum equation for composite material:
        \begin{equation}
            \label{CompositeMomentum}
            \pdiff{\sigma^*_{ij}}{x*_j} + \rho b^*_i =
            \rho\pdiff{^2u^*_i}{{t^*}^2} +
            \rho_\mathrm{fluid}\pdiff{q^*_i}{t^*}
        \end{equation}
    \item Momentum equation for pore fluid (Darcy's law)
        \begin{equation}
            \label{Darcy}
            -\pdiff{p^*_\mathrm{pore}}{x^*_i} + \rho_\mathrm{fluid}b^*_i -
            \frac{1}{k^*}q^*_i = \rho_\mathrm{fluid}\pdiff{^2u^*_i}{{t^*}^2} +
            \frac{\rho_\mathrm{fluid}}{n}\pdiff{q^*_i}{t^*}
        \end{equation}
    \item Constitutive equation for pore fluid:
        \begin{itemize}
            \item \sout{Compressible fluid} [not used --- not sure about how to handle \(\pdiff{w^*_j}{x^*_j}\)\ldots]:
                \begin{equation}
                    \label{PoreConstitutive}
                    \left[
                        p^*_\mathrm{pore} =
                        -Q\left(\alpha\pdiff{u^*_k}{x^*_k} +
                        \pdiff{w^*_k}{x^*_k}\right)
                    \right]
                    %\left[p^*_\mathrm{pore} = -Q \left(\alpha \frac{\partial u^*_k}{\partial x^*_k} +
                    %\frac{\partial w^*_k}{\partial x^*_k} \right)\right]
                \end{equation}
            \item
                \fbox{Incompressible fluid} (\(Q\to\infty\); constitutive eqn becomes
                kinematic constraint):
                \begin{equation}
                    \label{PoreConstitutiveIncompr}
                    \alpha\pdiff{\dot{u}^*_k}{x^*_k} + \pdiff{q^*_k}{x^*_k} = 0
                    %\alpha \frac{\partial u^*_k}{\partial x^*_k} +
                    %\frac{\partial w^*_k}{\partial x^*_k} = 0.
                \end{equation}
        \end{itemize}
    \item Constitutive equation for composite material:
        \begin{itemize}
            \item \fbox{Compressible solid}
                \begin{equation}
                    \label{SolidConstitutive}
                    \sigma^*_{ij} = \lambda^*e_{kk}\delta_{ij} + 2\mu^*e_{ij} -
                    \alpha\delta_{ij}p^*_\mathrm{pore},
                \end{equation}
                where
                \begin{equation*}
                    e_{ij} = \frac{1}{2} \left(\partial u^*_i/\partial x^*_j + \partial u^*_j/\partial x^*_i\right).
                \end{equation*}
            \item
                \sout{Incompressible solid} ($\lambda=\infty$; constitutive eqn becomes
                kinematic constraint) [Note: This is extremely unlikely since
                \(\lambda\) is the drained bulk modulus (see below)]:
                \begin{equation}
                    \sigma^*_{ij} =  -p^*_\mathrm{solid} \ \delta_{ij}+ 2\mu^* e_{ij} -
                    \alpha  p^*_\mathrm{pore} \ \delta_{ij},
                \end{equation}
                and pressure determined (indirectly) from
                \begin{equation}
                    \frac{\partial u^*_k}{\partial x^*_k} = 0.
                \end{equation}
        \end{itemize}
\end{itemize}

\section{Parameters}
\begin{itemize}
    \item \textbf{TODO} --- units of all quantities/parameters
    \item \(n\): porosity, i.e. the volume fraction occupied by the fluid
        (material is fully saturated).
    \item \(\rho_\mathrm{fluid}\): density of pore fluid.
    \item \(\rho\): density of the composite material; \(\rho =
        (1-n) \rho_\mathrm{solid} + n \rho_\mathrm{fluid}\) where
        \(\rho_\mathrm{solid}\) is the density of the solid skeleton material.
    \item \(\mathbf{b}^*\): body force (force per unit mass; e.g. gravitational
        acceleration).
    \item \(k^*\): Darcy permeability.
    \item \(\lambda^*, \mu^*\): Lamé parameters of drained material.
    \item \(Q, \alpha\): Poro-elasticity parameters.
        \begin{itemize}
            \item \(Q\) indicates the
                compressibility of the pore fluid (and we have \(Q=\infty\) for
                an incompressible pore fluid).
            \item \(\alpha\) is the ratio of fluid volume gained (or lost) in a
                material element due to the volume change of that element when
                loaded under the drained condition \citep[p. 225,
                under eqn. (6)]{cowin1999}.
        \end{itemize}
\end{itemize}

Parameter values to be supplied by Chris... Translation between
poro-elastic formulation and bi-phasic mixture theory (swindle)
is in \citet{simon1992}; the two formulations are completely equivalent
(see \citet{simon1992}, \citet{cowin1999} and references in both).

\section{Nondimensionalisation}
The following scalings will be used to nondimensionalise the equations:
\begin{align*}
    x^*_i=\mathcal{L}x_i,&\qquad \sigma^*_{ij}=E\sigma_{ij},\\
    u^*_i=\mathcal{L}u_i,&\qquad p^*_\mathrm{pore}=E p_\mathrm{pore},\\
    q^*_i=\frac{\mathcal{L}}{\mathcal{T}}q_i,&\qquad b^*_i=\frac{E}{\mathcal{L}\rho}b_i,\\
    t^*=\mathcal{T}t,&
\end{align*}
where \(\rho=(1-n)\rho_\mathrm{solid}+n\rho_\mathrm{fluid}\) is the compound
density of the poroelastic material, \(E\) is its Young's modulus and
\(\mathcal{L}\) and \(\mathcal{T}\) are problem specific length- and
time-scales, respectively.
We will also use the nondimensional Lamé parameters
\begin{equation}
    \qquad \lambda(\nu)=\frac{\lambda^*(E,\nu)}{E},\qquad
    \mu(\nu)=\frac{\mu^*(E,\nu)}{E},
\end{equation}
where the dependence on \(E\) is removed by division (\(E\) only appears as a
factor in both of the dimensional Lamé parameters.)

After substituting these scalings, the equations become:
\begin{itemize}
    \item Momentum equation for composite material:
        \begin{equation}
            \pdiff{\sigma_{ij}}{x_j} + b_i =
            \Lambda^2\left(\pdiff{^2u_i}{t^2} + \frac{\rho_\mathrm{fluid}/\rho_\mathrm{solid}}{1+n(\rho_\mathrm{fluid}/\rho_\mathrm{solid}-1)}\pdiff{q_i}{t}\right)
            \label{eqn:nondim_comp_mom}
        \end{equation}
    \item Momentum equation for pore fluid:
        \begin{equation}
            -\pdiff{p_\mathrm{pore}}{x_i}+\frac{\rho_\mathrm{fluid}/\rho_\mathrm{solid}}{1+n(\rho_\mathrm{fluid}/\rho_\mathrm{solid}-1)}b_i -
            \frac{1}{k}q_i =
            \frac{\rho_\mathrm{fluid}/\rho_\mathrm{solid}}{1+n(\rho_\mathrm{fluid}/\rho_\mathrm{solid}-1)}\Lambda^2\left(\pdiff{^2u_i}{t^2}+\frac{1}{n}\pdiff{q_i}{t}\right)
            \label{eqn:nondim_fluid_mom}
        \end{equation}
    \item Constitutive equation for pore fluid
        \begin{itemize}
            \item Incompressible
                \begin{equation}
                    \alpha\pdiff{\dot{u}_k}{x_k} + \pdiff{q_k}{x_k} = 0
                    \label{eqn:nondim_const_fluid}
                \end{equation}
        \end{itemize}
    \item Constitutive equation for composite material:
        \begin{itemize}
            \item Compressible solid:
                \begin{equation}
                    \sigma_{ij} = \lambda e_{kk}\delta_{ij} + 2\mu e_{ij} -
                    \alpha\delta_{ij}p_\mathrm{pore}
                    \label{eqn:nondim_const_comp}
                \end{equation}
        \end{itemize}
\end{itemize}
The new nondimensional parameters that arise are defined as:
\begin{align*}
    \Lambda=\frac{\mathcal{L}}{\mathcal{T}}\sqrt{\frac{\rho}{E}},&\qquad
    k=\frac{k^*E\mathcal{T}}{\mathcal{L}^2},\\
\end{align*}
along with the density ratio \(\rho_\mathrm{fluid}/\rho_\mathrm{solid}\).
The set of nondimensional parameters
\begin{equation}
    \boxed{\Lambda, k, \nu, \alpha, n, \rho_\mathrm{fluid}/\rho_\mathrm{solid}}
\end{equation}
is sufficient to determine the
poroelastic behaviour of the material in the above model (incompressible fluid,
compressible solid).

\section{Discretisation}
Each of the dependent variables \(\mathbf{u},\mathbf{q}\) and \(p\) must be
spatially discretised by approximation in an appropriate finite dimensional
subspace of the original function space in which they lie. The bases for these
finite dimensional subspaces will be denoted by \(\psi_j\) with a superscript to
distinguish those associated with one variable from those with another. We
denote the triangulation of the domain by \(\mathcal{T}_h\). Subscripts are
\(0\)-indexed to match the \texttt{C++} code.

\begin{itemize}
    \item Use the standard (scalar) quadratic Lagrangian basis on 6 node
        triangles to interpolate the spatial position:
        \begin{equation}
            x_i = \sum_{j=0}^5 X_{ij}\psi_j
            \label{eqn:x_interpolation}
        \end{equation}

\end{itemize}

\subsection{Solid displacement}
\begin{itemize}
    \item Equation determining the solid displacement,
        \eqref{eqn:nondim_comp_mom}, is generalised from linear elasticity
        \(\implies\) we can use a standard quadratic Lagrangian basis for the
        finite dimensional representation of \(\mathbf{u}\) in each element
        \(K\in\mathcal{T}_h\):
        \begin{equation}
            u_i = \sum_{j=0}^5 U_{ij}\psi^{[\mathbf{u}]}_j
            \label{eqn:u_interpolation}
        \end{equation}
        where we distinguish between the geometric shape functions \(\psi_j\)
        and \(\mathbf{u}\) shape functions \(\psi^{[\mathbf{u}]}_j\) for generality
        (although they are equal in this case).
        For more details of the discretisation of the composite momentum
        equation, refer to the \texttt{oomph-lib} linear elasticity element
        code since the discretisation is same, except for the addition of
        coupling terms and a modified stress tensor in our case.
\end{itemize}

\subsection{Pore fluid}

%The discretisation depends crucially on the question if the
%pore fluid is assumed to be compressible or incompressible
%[Chris: Your call; however, I assume that, given that the fluid comes
%``out of'' (or ``drains into'') the Navier-Stokes domain
%which is assumed to be occupied by an incompressible fluid, we'll go
%for the latter...].

A different discretisation is required for compressible and incompressible pore
fluids: in the compressible case, the pore pressure can be eliminated from the
momentum equation by substituting in the constitutive equation, but in the
incompressible case, we cannot eliminate the pressure. We're assuming
incompressible pore fluid.

\begin{itemize}
    \item Pore fluid momentum equation, \eqref{eqn:nondim_fluid_mom}, is a
        generalised Darcy's law \(\implies\) we must approximate the pore fluid
        flux \(\mathbf{q}\) in a finite dimensional subspace of
        \(H(\mathrm{div};K) =
        \{v\in L^2(K,\mathbb{R}^2) : \mathrm{div}\ v\in
        L^2(K,\mathbb{R})\}\) in each element
        \(K\).
        \begin{itemize}
            \item We use the Raviart-Thomas family of subspaces of
                \(H(\mathrm{div};K)\),
                \begin{equation*}
                    RT_k(K) = (P_k(K))^2 +
                    \mathbf{x}P_k(K),
                \end{equation*}
                on each element, where \(P_k(K)\) is the space of all
                \(k\)-th order polynomials on \(K\).
                The global approximation space is
                \begin{equation*}
                    RT_k(\Omega) =
                    \{\mathbf{u} : \mathbf{u}\in RT_k(K)\ \forall
                    K\in\mathcal{T}_h, \mathbf{u}\cdot\mathbf{n}_{ij}
                    \text{ is continuous across } e_{ij},\forall
                    e_{ij}\in\mathcal{E}_h\},
                \end{equation*}
                where \(\mathcal{E}_h = \{ e_{ij} = \partial K_i \cap \partial
                K_j, K_i,K_j\in \mathcal{T}_h\}\) (i.e. the set of internal
                edges.) and \(\mathbf{n}_{ij}\) is the unit normal across
                \(e_{ij}\).
                For details of the Raviart-Thomas spaces see \citet{monk2003};
                for an explicit example of their bases see \citet{ervin2012}.
            \item \(RT_k(K)\) has dimension \((k+1)(k+3)\). Explicitly:
                \begin{itemize}
                    \item \(\dim RT_0(K) = 3\) (3 edge functions; 0 internal
                        functions)
                    \item \(\dim RT_1(K) = 8\) (6 edge functions; 2 internal
                        functions)
                \end{itemize}
            \item The pore fluid flux is then interpolated in terms of the basis
                as
                \begin{align}
                    \mathbf{q} &= \sum_{j=0}^{N_\mathbf{q}-1} Q_j\bm{\psi}_{j}^{[\mathbf{q}]}\\
                    &= \sum_{j=0}^{N_\mathbf{q}^\mathrm{edge}-1}
                    Q_j\bm{\psi}_{j}^{[\mathbf{q}]}
                    + \sum_{j=N_\mathbf{q}^\mathrm{edge}}^{N_\mathbf{q}-1}
                    Q_j\bm{\psi}_{j}^{[\mathbf{q}]}
                    \label{eqn:q_interpolation}
                \end{align}
                \begin{itemize}
                    \item Note that the basis functions
                        \(\bm{\psi}_j^{[\mathbf{q}]}\) are vector-valued as
                        opposed to scalar, as is more common
                    \item The first \(N_\mathbf{q}^\mathrm{edge}\) values of \(Q_j\) are
                        stored as nodal data in the mid-edge node of the edge
                        which they are associated with
                    \item The remaining
                        \(N_\mathbf{q}^\mathrm{internal}=N_\mathbf{q}-N_\mathbf{q}^\mathrm{edge}\) values
                        of \(Q_j\) are stored as internal data in the element
                \end{itemize}
            \item \(RT_0\) and \(RT_1\) elements are implemented for Darcy flow
                and poroelasticity, and we are currently using \(RT_1\)
            \item Higher order elements would be very easy to add (just need to
                write template specialisations for a few functions ---
                \citet{ervin2012} gives an \(RT_2\) basis explicitly, and a
                scheme for computing bases for \(RT_k, \forall k\))
        \end{itemize}
    \item Pore fluid pressure is discretised using piecewise discontinuous
        polynomials of order up to \(k-1,\) where \(k\) is that in the \(RT_k\)
        discretisation of the pore fluid flux:
        \begin{equation}
            p_\mathrm{pore} = \sum_{j=0}^{N_p-1} P_j\psi_j^{[p]}
            \label{eqn:p_pore_interpolation}
        \end{equation}
        \begin{itemize}
            \item For \(k=0\), we have \(N_p=1\), i.e. piecewise constant
                pressure
            \item For \(k=1\), we have \(N_p=3\), i.e. piecewise linear pressure
        \end{itemize}
\end{itemize}

\section{Implementation of \(RT_k\) elements}
An implementation of \(RT_k\) elements requires more care than that of standard
Lagrange elements. \citet{rognes2009} detail the extra steps required for
\(RT_k\) elements if one is writing a finite element program from scratch.
Specifically, the authors suggest global and local numbering schemes for nodes
which allow the programmer to avoid further issues involving the orientation of
element edges. However, \texttt{Oomph-lib} already has its own numbering scheme,
and so we must work in the other direction, implementing \(RT_k\) elements via
various redirection schemes for the numbering of nodes and orientation of edges
and elements.  We use the bases for \(RT_0\) and \(RT_1\) given by
\citet{ervin2012}.

The structure of the new classes is as follows:
\begin{itemize}
    \item Element structure and maths common to all geometric elements is in
        \texttt{PoroelasticityEquations} class
    \item
        \begin{sloppypar}
            The \texttt{TPoroelasticityElement} class inherits the geometric basis
            from \texttt{TElement}; the maths from \texttt{PoroelasticityEquations}
            and provides the triangle-specific bases for \(RT_k\).
        \end{sloppypar}
\end{itemize}

\subsection{Reference element and storage scheme}
The basis functions are defined on the reference element \(\hat{K}\), pictured in
figure~\ref{fig:reference_element}.

\begin{figure}[ht!]
    \centering
    \begin{tikzpicture}[scale=2,label/.style=
            {
                postaction=
                {
                    decorate,
                    decoration=
                    {
                        markings,
                        mark=at position 0.5 with \node #1;
                    }
                }
            }]

        % define coordinates
        \coordinate (a) at (1,0);
        \coordinate (b) at (0,1);
        \coordinate (c) at (0,0);

        \draw [->,blue] (c) -- ++(1.3,0) node [right] {\(s_0\)};
        \draw [->,blue] (c) -- ++(0,1.3) node [above] {\(s_1\)};

        % draw the coords
        \node[below] at (a) {\((1,0)\)};
        \node[left] at (b) {\((0,1)\)};
        \node[below] at (c) {\((0,0)\)};

        % draw the nodes as blobs
        \fill (a) circle (0.04);
        \fill (b) circle (0.04);
        \fill (c) circle (0.04);

        % draw the lines
        \draw (a) -- (b) -- (c) -- cycle;

        \draw ({1/(2+sqrt(2))},{1/(2+sqrt(2))}) node {\(\hat{K}\)};

        \coordinate (A) at (3.7,0.5);
        \coordinate (B) at (3.6,1.5);
        \coordinate (C) at (2.9,0.4);

        \draw [->,blue] (2.5,0) -- ++(1.3,0) node [right] {\(x\)};
        \draw [->,blue] (2.5,0) -- ++(0,1.3) node [above] {\(y\)};

        \draw (A) -- (B) -- (C) -- cycle;

        % draw the nodes as blobs
        \fill (A) circle (0.04);
        \fill (B) circle (0.04);
        \fill (C) circle (0.04);

        \draw [->,red,dashed] (a) to[bend left] (A);
        \draw [->,red,dashed] (b) to[bend left] (B);
        \draw [->,red,dashed] (c) to[bend left] (C);

        \draw (3.4,0.75) node {\(K\)};

        \draw [->,label={[below]{\(F_K\)}}]
        (1.5,0.8) to [bend left] (2.3,0.8);
    \end{tikzpicture}
    \caption{\label{fig:reference_element}Reference element, \(\hat{K}\)}
\end{figure}

\begin{figure}[ht!]
    \centering
    \begin{subfigure}[b]{0.3\textwidth}
        \centering
        \begin{tikzpicture}[scale=2]
            % define coordinates
            \coordinate (a) at (1,0);
            \coordinate (b) at (0,1);
            \coordinate (c) at (0,0);
            \coordinate (d) at (0.5,0.5);
            \coordinate (e) at (0,0.5);
            \coordinate (f) at (0.5,0);

            \draw [->,blue] (c) -- ++(1.3,0) node [right] {\(s_0\)};
            \draw [->,blue] (c) -- ++(0,1.3) node [above] {\(s_1\)};

            % draw the coords
            \node[below] at (a) {\((1,0)\)};
            \node[left] at (b) {\((0,1)\)};
            \node[below] at (c) {\((0,0)\)};

            % draw the nodes as blobs
            \fill (a) circle (0.04);
            \fill (b) circle (0.04);
            \fill (c) circle (0.04);

            % draw the lines
            \draw (a) -- (b) -- (c) -- cycle;

            \draw [->] (d) -- ++({0.3/sqrt(2)},{0.3/sqrt(2)});
            \draw [->] (e) -- ++(-0.3,0);
            \draw [->] (f) -- ++(0,-0.3);
        \end{tikzpicture}
        \caption{\label{fig:RT_0_element}\(RT_0(\hat{K})\)}
    \end{subfigure}
    \begin{subfigure}[b]{0.3\textwidth}
        \centering
        \begin{tikzpicture}[scale=2]
            % define coordinates
            \coordinate (a) at (1,0);
            \coordinate (b) at (0,1);
            \coordinate (c) at (0,0);
            \coordinate (d0) at ({1/2+sqrt(3)/6},{1/2-sqrt(3)/6});
            \coordinate (d1) at ({1/2-sqrt(3)/6},{1/2+sqrt(3)/6});
            \coordinate (e0) at (0,{1/2+sqrt(3)/6});
            \coordinate (e1) at (0,{1/2-sqrt(3)/6});
            \coordinate (f0) at ({1/2-sqrt(3)/6},0);
            \coordinate (f1) at ({1/2+sqrt(3)/6},0);

            \draw [->,blue] (c) -- ++(1.3,0) node [right] {\(s_0\)};
            \draw [->,blue] (c) -- ++(0,1.3) node [above] {\(s_1\)};

            % draw the coords
            \node[below] at (a) {\((1,0)\)};
            \node[left] at (b) {\((0,1)\)};
            \node[below] at (c) {\((0,0)\)};

            % draw the nodes as blobs
            \fill (a) circle (0.04);
            \fill (b) circle (0.04);
            \fill (c) circle (0.04);

            % draw the lines
            \draw (a) -- (b) -- (c) -- cycle;

            \draw [->] (d0) -- ++({0.3/sqrt(2)},{0.3/sqrt(2)});
            \draw [->] (d1) -- ++({0.3/sqrt(2)},{0.3/sqrt(2)});
            \draw [->] (e0) -- ++(-0.3,0);
            \draw [->] (e1) -- ++(-0.3,0);
            \draw [->] (f0) -- ++(0,-0.3);
            \draw [->] (f1) -- ++(0,-0.3);

            \draw ({1/(2+sqrt(2))},{1/(2+sqrt(2))}) circle [radius={(0.15)}];
            \draw ({1/(2+sqrt(2))},{1/(2+sqrt(2))}) circle [radius={(0.2)}];
        \end{tikzpicture}
        \caption{\label{fig:RT_1_element}\(RT_1(\hat{K})\)}
    \end{subfigure}
    \caption{\label{fig:RT_k_elements}Degrees of freedom of \(RT_0(\hat{K})\) and \(RT_1(\hat{K})\) elements}
\end{figure}

\subsection{Mapping the basis from \(\hat{K}\) to \(K\)}
\begin{itemize}
    \item Let \(F_K\) be a nondegenerate map from the reference element
        \(\hat{K}\) to the specific element \(K\):
        \begin{equation*}
            F_K : \hat{K} \to F(\hat{K}) = K
        \end{equation*}
    \item Then define the map \(\mathcal{F}_K : H^m(\hat{K}) \to H^m(K)\) by
        \begin{equation*}
            \mathcal{F}_K(\phi) = \phi \circ F_K^{-1},
        \end{equation*}
        where \(\phi\in H^m(\hat{K})\). \(\mathcal{F}_K\) maps scalar functions
        (whose derivatives up to \(m\)-th order are square integrable)
        on the reference element \(\hat{K}\) to functions on the element \(K\).
    \item For finite elements based on \(H^m(K)\), \(\mathcal{F}_K\) is an
        appropriate way to map the basis functions from the reference element to
        the specific element since it is an isomorphism from \(H^m(\hat{K})\) to
        \(H^m(K)\)
    \item However, this is not the case for \(H(\mathrm{div})\) --- we need a
        map which also preserves continuity of normal components across element
        boundaries. This is the \emph{contravariant Piola map}:
        \begin{equation*}
            \mathcal{F}_K^\mathrm{div}(\phi) =
            \frac{1}{\det J}J\phi\circ F_K^{-1},
        \end{equation*}
        where \(\phi\in H(\mathrm{div})\) and where \(J=DF(X), X\in\hat{K}\) is
        the Jacobian of \(F_K\).
\end{itemize}

\subsection{Scaling of edge basis functions}
\begin{itemize}
    \item The reference element \(\hat{K}\) is not an equilateral triangle ---
        two of its edges have length \(1\) and the other has length \(\sqrt{2}\)
    \item The basis for \(RT_k(\hat{K})\) given by \citet{ervin2012} is such
        that the basis functions cannot be permuted among edges without
        modification due to the magnitude of each function depending on the
        length of the edge the function is associated with
    \item Therefore, we cannot arbitrarily choose the map \(F_K\) above, without
        further care, since there are restrictions edge-edge mappings from the
        reference element to the specific element
    \item To allow arbitrary mappings between edges, we can scale the local edge
        basis function \(\bm{\psi}^{[\mathbf{q}]}_j\) (for \(0\le j\le
        N_\mathbf{q}^\mathrm{edge}-1\)) after the mapping by the ratio
        \begin{equation*}
            \frac{\length_{K}(e_i)}{\length_{\hat{K}}(e_i)}
        \end{equation*}
    \item The basis functions associated with the internal degrees of freedom
        are not subject to this issue
    \item Note that we use the straight line distance between nodes to find the
        length of an edge of \(K\). This is not exact for elements with curved
        boundaries, but will converge to the right answer in the limit of
        infinite mesh refinement (although the rate of convergence must be
        verified)
\end{itemize}

\subsection{Assignment of edge normal vector signs}
\begin{itemize}
    \item The edge degrees of freedom can be taken to be the normal component of
        the flux \(\mathbf{q}\) along the edges at each of the so-called ``Gauss
        points''
    \item Since we are dealing with normal components of a vector function, we
        must have a consistently defined unit normal vector at element-element
        boundaries. I.e. shared edges of neighbouring elements must agree on the
        normal vector so that the flux across such edges is well defined
    \item To ensure this, as a preprocessing step just after meshing, we need to
        assign to each edge the appropriate sign (which must be done on both
        sides of the edge)
    \item The following algorithm is used, where \(\sgn(e_{K,i})\) denotes the
        sign of the \(i\)-th edge of element \(K_i\) (note that, unlike in the
        above edge notation, we treat each interior edge twice, once with
        respect to each element that share it):
        \begin{itemize}
            \item Loop over all elements \(K\in\mathcal{T}_h\)
                \begin{itemize}
                    \item By default, \(\sgn(e_{K,i})=+1\) for all edges \(i\)
                        of \(K\)
                    \item Now traverse all edges \(e_{K,i}\) of \(K\)
                        \begin{itemize}
                            \item If edge \(e_{ij}\) has previously been
                                traversed, do not change its sign
                            \item If edge \(e_{ij}\) has not previously been
                                traversed, change \(\sgn(e_{K,i})\) to \(-1\)
                        \end{itemize}
                \end{itemize}
        \end{itemize}
        \begin{figure}[ht!]
            \centering
            \begin{tikzpicture}[scale=2]
                \draw
                (0,0) -- (0,1) node[pos=0.5,left] {\(+\)}
                               node[pos=0.5,right]{\(-\)}
                      -- (1,0) node[pos=0.5,left] {\(+\)}
                               node[pos=0.5,right]{\(-\)}
                      -- (0,0) node[pos=0.5,above]{\(-\)}
                               node[pos=0.5,below]{\(+\)};
                \draw
                (1,0) -- (1.8,1.1) node[pos=0.5,left] {\(+\)}
                                   node[pos=0.5,right]{\(-\)}
                      -- (0,1)     node[pos=0.5,above]{\(+\)}
                                   node[pos=0.5,below]{\(-\)};
                \draw
                (1.8,1.1) -- (0.7,1.75) node[pos=0.5,above]{\(+\)}
                                        node[pos=0.5,below]{\(-\)}
                          -- (0,1)      node[pos=0.5,left] {\(+\)}
                                        node[pos=0.5,right]{\(-\)};
                \draw
                (1.8,1.1) -- (1.9,-0.2) node[pos=0.5,left] {\(+\)}
                                        node[pos=0.5,right]{\(-\)}
                          -- (1,0)      node[pos=0.5,above]{\(+\)}
                                        node[pos=0.5,below]{\(-\)};
            \end{tikzpicture}
            \caption{\label{fig:edge_signs}Example of edge sign assignments}
        \end{figure}
\end{itemize}

\subsection{Reversing direction of edges}
\begin{itemize}
    \item We also need to ensure that neighbouring elements agree on the
        direction of the edge they share so that shared degrees of freedom along
        that edge are correctly interpreted
    \item This is done by reflecting the ``Gauss points'' along that edge in one
        of the two elements
    \item \texttt{Oomph-lib}'s unstructured meshes, created by \texttt{Triangle}
        \citep{shewchuk96b}, have anticlockwise orientated elements (assuming no
        element inversion occurs\ldots) and so we must reverse the direction of
        exactly the edges whose sign must also be changed
    \item This is implemented as a redirection around the accessor function for
        the location of the Gauss points which simply reflects them around the
        centre of the edge if the sign of that edge (in the current element) is
        \(-1\)
\end{itemize}

\section{Code and examples}
The demo drivers of the Darcy and poroelasticity elements can be found in the
usual directories:
\begin{itemize}
    \item Darcy:
        \texttt{demo\_drivers/darcy/unstructured\_two\_d\_circle/unstructured\_two\_d\_circle.cc}
    \item Poroelasticity:\\
        \texttt{demo\_drivers/poroelasticity/unstructured\_two\_d\_annular\_sector/unstructured\_two\_d\_annular\_sector.cc}

\end{itemize}
\texttt{src/darcy} and \texttt{src/poroelasticity}; the driver codes are in
\texttt{demo\_drivers/darcy} and \texttt{src/poroelasticity}.
\subsection{Darcy}
\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.8\textwidth]{darcy.png}
    \caption{\label{fig:darcy}Example solution of Darcy equations. Flux
    \(\mathbf{q}\) plotted as a vector field, coloured by the fluid pressure}
\end{figure}

\FloatBarrier

\subsection{Poroelasticity}
\begin{figure}[ht!]
    \centering
    \includegraphics[width=0.8\textwidth]{poroelasticity.png}
    \caption{\label{fig:poroelasticity}Example solution of poroelasticity
equations. Clockwise from top left: \(u_x\), \(u_y\), \(q_y\), \(p\), \(q_x\)}
\end{figure}

\FloatBarrier

%\section{Code}
%
%\subsection{Compressible pore fluid}
%Eliminate pore pressure by inserting (\ref{PoreConstitutive})
%into (\ref{Darcy}) and (\ref{SolidConstitutive}) (which itself
%ends up in (\ref{CompositeMomentum})) $\Longrightarrow$
%Two coupled second-order PDEs for ${\bf u}$ and ${\bf w}$,
%discretised by standard isoparametric Galerkin expansion
%\begin{equation}
%    x_i = \sum X_{ij} \psi_j \ \ \ \
%    u_i = \sum U_{ij} \psi_j \ \ \ \
%w_i = \sum W_{ij} \psi_j
%\end{equation}
%with time-derivatives from Newmark, say. Note that time-derivatives
%of ${\bf u}$ and ${\bf w}$ appear in both equations. Zienkiewicz and
%Shiomi (1984) make a big deal of this (and suggest a change of variables
%to uncouple the inertia terms) but I don't think it matters for us.
%
%Note also that if inertial time-derivatives are neglected, we can reduce
%the equations to a Navier-Lamé equation for the solid displacements (which
%includes a pore-pressure-dependent source term) and an unsteady heat equation
%for the pressure (forced by the divergence of the solid displacement),
%as described in the poro-elasiticy ``web notes''. Unfortunately, this
%now seems to be unlikely to be what we want... [Chris: your call, again.]
%
%\subsection{Incompressible pore fluid}
%Pore pressure cannot be eliminated but is determined by the
%kinematic constraint (\ref{PoreConstitutiveIncompr}).
%Saddle point structure of resulting equation requires
%different (lower-order) discretisation for pressure,
%\begin{equation}
%p_{\rm pore} = \sum P_j \psi^{[p]}_j
%\end{equation}
%where $(\psi_j, \psi^{[p]}_j)$ are an LBB stable pair of
%basis functions, e.g. Taylor Hood.
%
%\section{Questions}
%\begin{itemize}
%    \item Do we want the pore fluid to be incompressible? [yes]
%    \item If not: Do we want inertial time-derivatives in the model
%        [yes]. If so, we can't go for the originally planned ``Navier-Lamé + unsteady
%        heat equation for pressure'' approach described on the poro-elasticity
%        web notes (and easily implemented by multiple inheritance; what a shame...).
%    \item What are sensible estimates for the parameters?
%\end{itemize}

%\section{What next}
%Once we (and/or Chris) have decided on the answers to the
%above question, actually go through the proper weak-formification
%of the relevant equations which will also clarify the boundary
%conditions. Then code it all up...

\bibliography{Poroelasticity}

\end{document}
